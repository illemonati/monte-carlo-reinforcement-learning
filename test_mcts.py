import tictactoe
import mcts

state = tictactoe.TicTacToeState()

human_player = input("Do you want to be 1 or 2: ")
human_player = int(human_player)

while state.get_winner() < 0:
    state.render()
    print(state.get_possible_actions())
    if state.turn_player == human_player:
        action = int(input("Your move: "))
    else:
        root = mcts.MCTSNode(state)
        mcts.monte_carlo_tree_search(root, iterations=10000, debug=False)
        print(root.child_nodes)
        print(root.child_scores(enable_exploration=False))
        action = root.get_action_with_most_visits()
    state.act(action)

state.render()

print("Winner is: ", state.get_winner())
