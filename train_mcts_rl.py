import os

import torch.optim
import torch.multiprocessing as mp

import tictactoe
import mcts_with_rl
from policy_value_network import PolicyValueNetwork, SimplePolicyValueNetwork
import numpy as np
from collections import deque
import random

# device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

device = torch.device('cuda')


class MCTS_Agent:
    def __init__(self, network):
        self.network = network
        self.search_probabilities = None
        self.tag = "MCTS_Agent"

    def set_tag(self, tag):
        self.tag = tag

    def __str__(self):
        return self.tag

    def act(self, state, randomness=0.2, search_steps_per_turn=25, temperature=1.25, enable_noise=False):
        root = mcts_with_rl.MCTSNode(state, self.network, device=device, enable_noise=enable_noise)
        mcts_with_rl.monte_carlo_tree_search(root, search_steps_per_turn)
        action = root.choose_action_to_play(temperature=temperature, randomness=randomness)
        self.search_probabilities = root.get_search_probabilities()
        return action


def battle(agent1: MCTS_Agent, agent2: MCTS_Agent, battles=100, render=False, randomness=0.2, search_steps_per_turn=25,
           temperature=1.25, set_tags=True, enable_noise=False,
           print_every=20, render_last=False, debug=False, process_num=None, return_queue=None,
           score_decay_coefficient=1):
    states = []
    scores = []
    search_probabilities = []

    p1_agent = agent1
    p2_agent = agent2

    if set_tags:
        agent1.set_tag("agent1")
        agent2.set_tag("agent2")

    p1_wins = 0
    p2_wins = 0
    agent1_wins = 0
    agent2_wins = 0
    draws = 0


    # this doesn't actually get the real search probabilities for the leaf state, just placeholder since any is okay
    dummy_probs = np.full(9, 1 / 9)

    turn_agent = None

    for ep in range(battles):
        state = tictactoe.TicTacToeState()
        states_this_ep = []
        search_probabilities_this_ep = []
        winner = state.get_winner()
        while winner < 0:
            if render or (ep == battles - 1 and render_last):
                state.render()
            if state.turn_player == 1:
                turn_agent = p1_agent
            else:
                turn_agent = p2_agent
            action = turn_agent.act(state, randomness=randomness, search_steps_per_turn=search_steps_per_turn,
                                    temperature=temperature, enable_noise=enable_noise)

            states_this_ep.append(state.board.copy())
            search_probabilities_this_ep.append(turn_agent.search_probabilities)

            state.act(action)
            winner = state.get_winner()

        if render or (ep == battles - 1 and render_last):
            state.render()

        states_this_ep.append(state.board)
        search_probabilities_this_ep.append(dummy_probs)

        if winner == 0:
            score = 0
        else:
            score = 1 if winner == 1 else -1
            # score = -1 if winner == 1 else 1

        for i in range(len(states_this_ep)):
            states.append(states_this_ep[-i - 1])
            scores.append(score)
            search_probabilities.append(search_probabilities_this_ep[-i - 1])
            score *= score_decay_coefficient

        # print(states, scores, search_probabilities)

        if winner == 0:
            draws += 1
        else:
            if turn_agent == agent1:
                agent1_wins += 1
            else:
                agent2_wins += 1
            if winner == 1:
                p1_wins += 1
            else:
                p2_wins += 1

        if ep == 0 or ep % print_every == print_every - 1:
            if debug:
                print(states_this_ep, scores[-len(states_this_ep):], search_probabilities_this_ep)

            if process_num is not None:
                print(f"process: {process_num}", end=" ")
            print(
                f"ep: {ep}, winner: {winner}, p1: {p1_agent}, p2: {p2_agent}, p1w: {p1_wins}, p2w: {p2_wins}, d: {draws}, a1w: {agent1_wins}, a2w: {agent2_wins}")

        p1_agent, p2_agent = p2_agent, p1_agent

    if return_queue is None:
        return states, scores, search_probabilities, p1_wins, p2_wins, draws, agent1_wins, agent2_wins
    else:
        return_queue.put((states, scores, search_probabilities, p1_wins, p2_wins, draws, agent1_wins, agent2_wins))


def train(states, scores, search_probabilities, network: PolicyValueNetwork, batch_size=256,
          steps=500, save_num=None, step_print_every=20, end_batch_only=True, folder_name="networks"):
    optimizer = torch.optim.Adam(network.parameters(), lr=1e-3)
    policy_loss_fn = torch.nn.CrossEntropyLoss()
    value_loss_fn = torch.nn.MSELoss()
    num_batches = int(np.ceil(len(states) / batch_size))

    states = torch.from_numpy(np.array(states)).float().to(device)
    search_probabilities = torch.from_numpy(np.array(search_probabilities)).float().to(device)
    scores = torch.from_numpy(np.array(scores)).float().to(device)

    for step in range(steps):
        step_loss = 0
        for batch_num in range(num_batches):
            batch_start = batch_num * batch_size
            batch_end = batch_start + batch_size
            batch_end = min(batch_end, len(states))

            optimizer.zero_grad()

            batch_states = states[batch_start:batch_end]
            batch_scores = scores[batch_start:batch_end]
            batch_search_probabilities = search_probabilities[batch_start:batch_end]

            batch_prior_probabilities, batch_values = network(batch_states)

            policy_loss = policy_loss_fn(batch_prior_probabilities, batch_search_probabilities)
            value_loss = value_loss_fn(batch_values, batch_scores)
            batch_loss = (policy_loss + value_loss)
            batch_loss.backward()
            optimizer.step()

            batch_loss_item = batch_loss.item()
            # this isn't entirely accurate cause last batch has diff length but doesn't matter too much
            step_loss += batch_loss_item / num_batches
            if step == 0 or step % step_print_every == step_print_every - 1:
                if not end_batch_only or batch_num == num_batches - 1:
                    print(f"[step: {step}][batch: {batch_num}] loss: {batch_loss_item} step_loss: {step_loss}")

    print(f"saving network-{save_num}.pt")
    torch.save(network.state_dict(), os.path.join(folder_name, f"network-{save_num}.pt"))


def main_loop(iterations=10000, previous_network_dict=None, save_num_start=0, folder_name=None, num_processes=8,
              use_simple=True, max_memory=1000):
    if use_simple:
        current_network = SimplePolicyValueNetwork().to(device)
        target_network = SimplePolicyValueNetwork().to(device)
    else:
        current_network = PolicyValueNetwork().to(device)
        target_network = PolicyValueNetwork().to(device)

    mp.set_start_method('spawn')
    if previous_network_dict is not None:
        current_network.load_state_dict(previous_network_dict)
        target_network.load_state_dict(previous_network_dict)

    current_network.share_memory()
    current_network.eval()

    reset = True

    for iteration in range(iterations):
        print(f"starting iteration: {iteration}")

        processes = []
        queues = []

        p1_wins = 0
        p2_wins = 0
        draws = 0
        agent1_wins = 0
        agent2_wins = 0

        if reset:
            states = deque(maxlen=max_memory)
            scores = deque(maxlen=max_memory)
            search_probabilities = deque(maxlen=max_memory)


        for p_num in range(num_processes):
            p_agent1 = MCTS_Agent(current_network)
            p_agent2 = MCTS_Agent(current_network)
            p_queue = mp.Queue()
            queues.append(p_queue)
            p = mp.Process(target=battle, args=(p_agent1, p_agent2), kwargs={
                "battles": 50,
                "randomness": 0,
                "temperature": 1.5,
                "return_queue": p_queue,
                "process_num": p_num,
                "enable_noise": True
            })
            p.start()
            processes.append(p)

        for i, p in enumerate(processes):
            (p_states, p_scores, p_search_probabilities, p_p1_wins, p_p2_wins, p_draws, p_agent1_wins,
             p_agent2_wins) = queues[i].get()
            states.extend(p_states)
            scores.extend(p_scores)
            search_probabilities.extend(p_search_probabilities)
            p1_wins += p_p1_wins
            p2_wins += p_p2_wins
            draws += p_draws
            agent1_wins += p_agent1_wins
            agent2_wins += p_agent2_wins
            p.join()

        print(
            f"iteration {iteration} battles done, p1w: {p1_wins}, p2w: {p2_wins}, d: {draws}, a1w: {agent1_wins}. a2w: {agent2_wins}")

        train(states, scores, search_probabilities, target_network, save_num=iteration + save_num_start, steps=1000,
              batch_size=512,
              folder_name=folder_name)

        agent1 = MCTS_Agent(current_network)
        agent2 = MCTS_Agent(target_network)

        agent1.set_tag("current network")
        agent2.set_tag("target network")

        _, _, _, p1_wins, p2_wins, draws, current_network_wins, target_network_wins = battle(
            agent1, agent2,
            battles=20,
            randomness=0,
            temperature=1,
            search_steps_per_turn=50,
            set_tags=False,
            print_every=10,
            render_last=True,
            enable_noise=False

        )
        print(
            f"iteration {iteration} eval battles done,  p1w: {p1_wins}, p2w: {p2_wins}, d: {draws}, cw: {current_network_wins}. tw: {target_network_wins}")
        agent2.network = current_network
        if target_network_wins + int(draws/2) >= current_network_wins:
            print("replacing network")
            current_network.load_state_dict(target_network.state_dict())
            current_network.share_memory()
            current_network.eval()
            reset = True
        else:
            print("maintaining network")
            reset = False


if __name__ == "__main__":
    old_save = torch.load('./simple-network/network-33.pt')
    # old_save = None
    main_loop(previous_network_dict=old_save, save_num_start=34, folder_name="simple-network",
              num_processes=8)
