from __future__ import annotations
import numpy as np
import math
import pickle


def monte_carlo_tree_search(node: MCTSNode, iterations=10000, debug=False):
    for iteration in range(iterations):
        if debug:
            if iteration % 100 == 99:
                print(f"iteration [{iteration + 1}]")
        selected_node = node.selection()
        # print(selected_node.is_leaf_node)
        # print(selected_node.state.board)
        # print(selected_node.is_fully_expanded)
        score = selected_node.simulation()
        selected_node.back_propagation(score)


def load_mcts_node(path):
    return pickle.load(open(path, 'rb'))

class MCTSNode:
    def __init__(self, state, parent=None):
        self.state = state
        self.parent = parent

        # 0 is draw, 1 and 2 are player wins, -1 is not finished
        if self.state.get_winner() > -1:
            self.is_leaf_node = True
        else:
            self.is_leaf_node = False

        # if this is a leaf, then by definition there are 0 children
        self.is_fully_expanded = self.is_leaf_node

        # sum of simulation results, also called w
        self.total_simulation_reward = 0

        # number of times node has been visited through back propagation
        self.total_number_of_visits = 0

        self.exploration_coefficient = 2

        self.child_nodes = [None] * state.action_size

        self.unexplored_actions = list(self.state.get_possible_actions())

    def save(self, path):
        pickle.dump(self, open(path, 'wb'))

    def selection(self):
        if self.is_leaf_node:
            return self
        if self.is_fully_expanded:
            return self.get_best_child().selection()
        else:
            return self.expansion()

    def child_scores(self, enable_exploration=True):
        score_multiplier = 1 if self.state.turn_player == 1 else -1
        child_scores = np.array(
            [score_multiplier * self.UCBT(child_node, enable_exploration) if child_node is not None else (-np.inf) for
             child_node in
             self.child_nodes])
        return child_scores


    def get_action_with_most_visits(self):
        return max(range(len(self.child_nodes)), key=lambda i: self.child_nodes[i].total_number_of_visits if self.child_nodes[i] is not None else -np.inf)

    def get_best_action(self, enable_exploration=True):
        child_scores = self.child_scores()

        selected_index = np.random.choice(np.flatnonzero(child_scores == child_scores.max()))
        return selected_index

    def get_best_child(self, enable_exploration=True):
        return self.child_nodes[self.get_best_action(enable_exploration=enable_exploration)]

    def expansion(self):
        # note this is vulnerable to loops
        # the unexplored state could be same as current state
        # or there could be multiple actions that lead to same state
        # we don't check these for now since our game doesn't require it
        action = self.unexplored_actions.pop()
        state = self.state.simulate_action(action)
        # print(self.state.board, action, state.board)
        node = MCTSNode(state, self)
        self.child_nodes[action] = node
        if len(self.unexplored_actions) == 0:
            self.is_fully_expanded = True
        return node

    def simulation(self):
        winner = self.state.get_winner()
        copy_state = self.state.copy()
        while winner == -1:
            copy_state.act_randomly()
            winner = copy_state.get_winner()
        if winner == 0:
            return 0
        elif winner == 1:
            return 1
        elif winner == 2:
            return -1

    def back_propagation(self, score):
        self.total_number_of_visits += 1
        self.total_simulation_reward += score
        if self.parent is not None:
            self.parent.back_propagation(score)

    def UCBT(self, child_node: MCTSNode, enable_exploration=True):
        # reward / visits
        # similar to a win/lose rate, estimates win ratio
        exploitation_component = child_node.total_simulation_reward / child_node.total_number_of_visits
        result = exploitation_component
        if enable_exploration:
            exploration_component = self.exploration_coefficient * (
                    math.log(self.total_number_of_visits) / child_node.total_number_of_visits) ** (1 / 2)
            result += exploration_component

        return result
