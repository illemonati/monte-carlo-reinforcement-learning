import torch

import tictactoe
import policy_value_network
import mcts_with_rl

state = tictactoe.TicTacToeState()

device = torch.device('cuda')

network = policy_value_network.SimplePolicyValueNetwork()
network.load_state_dict(torch.load('simple-network/network-8.pt'))

human_player = input("Do you want to be 1 or 2: ")
human_player = int(human_player)

while state.get_winner() < 0:
    state.render()
    print(state.get_possible_actions())
    if state.turn_player == human_player:
        action = int(input("Your move: "))
    else:
        root = mcts_with_rl.MCTSNode(state, network, device=device, enable_noise=True)
        mcts_with_rl.monte_carlo_tree_search(root, iterations=100, debug=False)
        print(root.child_nodes)
        print(root.value)
        print(root.child_scores())
        print(root.get_search_probabilities())
        print(root.prior_probabilities)
        print([x.value if x is not None else None for x in root.child_nodes])
        action = root.choose_action_to_play(temperature=1.25)
    state.act(action)

state.render()

print("Winner is: ", state.get_winner())
