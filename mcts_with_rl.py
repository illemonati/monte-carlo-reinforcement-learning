from __future__ import annotations
import numpy as np
import math
import pickle

import torch

from policy_value_network import PolicyValueNetwork
import random


def monte_carlo_tree_search(node: MCTSNode, iterations=100, debug=False):
    for iteration in range(iterations):
        if debug:
            if iteration % 100 == 99:
                print(f"iteration [{iteration + 1}]")
        selected_node = node.selection()
        if not selected_node.is_leaf_node:
            selected_node.expansion()
        # score = selected_node.simulation()
        selected_node.back_propagation()


def load_mcts_node(path):
    return pickle.load(open(path, 'rb'))


class MCTSNode:
    def __init__(self, state, network: PolicyValueNetwork, parent=None, device=None, enable_noise=True):
        self.state = state
        self.enable_noise = enable_noise
        self.parent = parent
        self.network = network
        self.device = device if device is not None else torch.device(
            'cuda') if torch.cuda.is_available() else torch.device('cpu')
        self.network.to(self.device)

        # 0 is draw, 1 and 2 are player wins, -1 is not finished
        winner = self.state.get_winner()
        if winner > -1:
            self.is_leaf_node = True
        else:
            self.is_leaf_node = False

        self.is_fully_expanded = False

        # sum of simulation results, also called w
        self.total_simulation_reward = 0

        # number of times node has been visited through back propagation, also called n
        self.total_number_of_visits = 0

        self.dirichlet_noise = torch.distributions.dirichlet.Dirichlet(torch.full((state.action_size,), 0.3).to(device))

        self.exploration_control = 1
        self.exploration_coefficient_init = 1
        self.exploration_coefficient_puct = 2 ** (1 / 2)
        self.score_multiplier = 1 if self.state.turn_player == 1 else -1

        self.child_nodes = [None] * state.action_size

        self.prior_probabilities, self.value = self.get_prior_probabilities_and_value()

        # if self.is_leaf_node:
        #     print(state.board, state.turn_player, winner, self.value)

        if winner == 0:
            self.value = 0
        elif winner == 1:
            self.value = 1
        elif winner == 2:
            self.value = -1


        # if self.is_leaf_node:
        #     print(winner, self.value)

        # self.expansion()

    def save(self, path):
        pickle.dump(self, open(path, 'wb'))

    def selection(self):
        # here leaf nodes don't count as expanded
        if self.is_fully_expanded:
            return self.get_best_child().selection()
        return self

    def child_scores(self, enable_exploration=True):

        child_scores = np.array(
            [self.P_UCBT(child_node,
                         self.prior_probabilities[i]) if child_node is not None else (
                -np.inf) for
             (i, child_node) in
             enumerate(self.child_nodes)])
        for i in self.state.get_impossible_actions():
            child_scores[i] = -np.inf
        return child_scores

    def choose_action_to_play(self, temperature=1.25, randomness=0):
        if random.random() < randomness:
            return random.choice(self.state.get_possible_actions())
        bottom = self.total_number_of_visits ** (1 / temperature)
        probabilities = [child_node.total_number_of_visits ** (1 / temperature) / bottom
                         if child_node is not None and i in self.state.get_possible_actions()
                         else -np.inf for (i, child_node) in enumerate(self.child_nodes)]
        return np.argmax(probabilities)

    def get_best_action(self, enable_exploration=True):
        child_scores = self.child_scores()

        # selected_index = np.argmax(child_scores)
        selected_index = np.random.choice(np.flatnonzero(child_scores == child_scores.max()))
        return selected_index

    def get_best_child(self, enable_exploration=True):
        return self.child_nodes[self.get_best_action(enable_exploration=enable_exploration)]

    def get_prior_probabilities_and_value(self):
        # gets prior probability for all actions with this state, so P(s, a) for all a
        # and the value of that action, also known as Q(s, a) for all a
        with torch.no_grad():
            board_tensor = torch.from_numpy(self.state.board).float().unsqueeze(0).to(self.device)
            policy, value = self.network(board_tensor)
            # print(policy.device, self.dirichlet_noise.device)
            if self.enable_noise:
                policy += self.dirichlet_noise.sample()

        # print(policy, value)
        return policy[0].cpu(), value[0].cpu()
        # return torch.distributions.dirichlet.Dirichlet(policy[0]).sample().cpu(), value[0].cpu()

    def expansion(self):
        if self.is_leaf_node or self.is_fully_expanded:
            return self
        for action in self.state.get_possible_actions():
            state = self.state.simulate_action(action)
            new_node = MCTSNode(state, self.network, parent=self, device=self.device, enable_noise=self.enable_noise)
            self.child_nodes[action] = new_node
        self.is_fully_expanded = True
        return self

    def get_search_probabilities(self):

        return [child_node.total_number_of_visits / (self.total_number_of_visits - 1)
                if child_node is not None and i in self.state.get_possible_actions()
                else 0 for (i, child_node) in enumerate(self.child_nodes)]

    def back_propagation(self, value=None, debug=False):
        if value is None:
            value = self.value

        if debug:
            print(value)

        self.total_number_of_visits += 1
        self.total_simulation_reward += value
        if self.parent is not None:
            self.parent.back_propagation(value=value)

    def P_UCBT(self, child_node, prior_probability_p):
        action_value_q = self.score_multiplier * child_node.total_simulation_reward / max(
            child_node.total_number_of_visits, 1)
        exploitation_component = action_value_q

        sum_children_visits = max(self.total_number_of_visits - 1, 1)

        # print(self.total_number_of_visits, [c.total_number_of_visits for c in self.child_nodes])

        exploration_coefficient = np.log(
            1 + sum_children_visits + self.exploration_coefficient_puct) / self.exploration_coefficient_puct + self.exploration_coefficient_init

        exploration_coefficient = self.exploration_control * exploration_coefficient

        exploration_component = (exploration_coefficient
                                 * prior_probability_p * ((self.total_number_of_visits - 1) ** (1 / 2)) /
                                 (1 + child_node.total_number_of_visits))

        return exploitation_component + exploration_component
