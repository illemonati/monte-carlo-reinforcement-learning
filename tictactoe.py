import copy

import numpy as np
import random

class TicTacToeState:
    def __init__(self):
        # 2 players, 1 and 2
        # 0 is if no player
        self.turn_player = 1
        self.board = np.zeros((3, 3))
        self.action_size = 9

    def render(self):
        print('------------------------')
        print(self.board)
        print('------------------------')

    @staticmethod
    def action_to_board_position(action: int):
        return divmod(action, 3)

    def get_possible_actions(self):
        return np.where(self.board.flatten() == 0)[0]

    def get_impossible_actions(self):
        return np.where(self.board.flatten() > 0)[0]

    def simulate_action(self, action):
        simulated_state = self.copy()
        simulated_state.act(action)
        return simulated_state

    def act_randomly(self):
        action = random.choice(self.get_possible_actions())
        self.act(action)
    def act(self, action):
        row, col = self.action_to_board_position(action)
        self.board[row][col] = self.turn_player
        self.turn_player = 2 if self.turn_player == 1 else 1

    def copy(self):
        new_state = TicTacToeState()
        new_state.turn_player = self.turn_player
        new_state.board = np.array(self.board)
        return new_state

    def get_winner(self):
        # returns -1 if not finished, 0 if draw, 1 or 2 if player win

        # check for row and col wins
        for i in range(3):
            if (self.board[i][0] != 0) and (self.board[i] == self.board[i][0]).all():
                return self.board[i][0]
            if (self.board[0][i] != 0) and (self.board[:, i] == self.board[0][i]).all():
                return self.board[0][i]
        # check diag
        if (self.board[0][0] != 0) and (np.diag(self.board) == self.board[0][0]).all():
            return self.board[0][0]
        if (self.board[0][2] != 0) and (np.diag(np.fliplr(self.board)) == self.board[0][2]).all():
            return self.board[0][2]

        # check if draw
        if (self.board > 0).all():
            return 0

        return -1

    def __eq__(self, other):
        return (self.board == other.board).all()
