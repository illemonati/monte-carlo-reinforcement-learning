import torch
import torch.nn as nn
import torch.nn.functional as F


class TransformerBlock(nn.Module):
    def __init__(self, embed_size, heads=3, forward_expansion=2):
        super().__init__()
        self.embed_size = embed_size
        self.heads = heads
        self.forward_expansion = forward_expansion

        self.attention = nn.MultiheadAttention(embed_size, heads, batch_first=True, dropout=0.3)
        self.norm1 = nn.LayerNorm(embed_size)
        self.feed_forward = nn.Sequential(
            nn.Linear(embed_size, embed_size * forward_expansion),
            nn.Tanh(),
            nn.Linear(embed_size * forward_expansion, embed_size)
        )
        self.norm2 = nn.LayerNorm(embed_size)

    def forward(self, x):
        # print(x.requires_grad)
        attention = self.attention(x, x, x)[0]
        x = self.norm1(x + attention)
        forward = self.feed_forward(x)
        x = self.norm2(x + forward)
        return x


class PolicyValueNetwork(nn.Module):
    def __init__(self, input_size=9, policy_size=9, embed_size=9, blocks=3):
        super().__init__()
        self.input_size = input_size
        self.policy_size = policy_size
        self.value_size = 1

        self.flatten = nn.Flatten()
        self.input_embed = nn.Parameter(torch.randn(input_size, embed_size))
        # no positional embeddings because we really don't need them
        self.transformer_encoder = nn.Sequential(
            *(
                TransformerBlock(embed_size) for _ in range(blocks)
            )
        )
        self.flatten2 = nn.Flatten()

        self.simple_decoder = nn.Linear(embed_size, policy_size + self.value_size)

    def forward(self, x):
        x = self.flatten(x)
        x = torch.matmul(x, self.input_embed)
        x = self.transformer_encoder(x)
        x = self.flatten(x)
        x = self.simple_decoder(x)
        value, policy = x[:, 0], x[:, 1:]
        value = F.tanh(value)
        policy = F.softmax(policy, 1)
        return policy, value


# this is a simple one for fast training because transformer pretty slow on my limited hardware
class SimplePolicyValueNetwork(nn.Module):
    def __init__(self):
        super().__init__()

        self.policy_size = 9
        self.value_size = 1

        self.conv1 = nn.Conv2d(1, 3, 3, padding=2)
        self.conv2 = nn.Conv2d(3, 3, 3, padding=1)
        self.conv3 = nn.Conv2d(3, 3, 3)
        self.flat = nn.Flatten()
        self.fc1 = nn.Linear(27, 512)
        self.fc2 = nn.Linear(512, 256)
        self.out = nn.Linear(256, self.policy_size + self.value_size)

    def forward(self, x):
        # changing 2 to -1 helps possible
        x[x == 2] = -1
        x = torch.unsqueeze(x, 1)
        # print(x.shape)
        x = F.relu(self.conv1(x))
        x = F.relu(self.conv2(x))
        x = F.relu(self.conv3(x))
        x = self.flat(x)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = F.relu(self.out(x))
        value, policy = x[:, 0], x[:, 1:]
        # value = F.tanh(value)
        # policy = F.softmax(policy, 1)
        return policy, value
